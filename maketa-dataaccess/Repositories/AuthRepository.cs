﻿using System;
using System.Threading.Tasks;
using maketa_dataaccess.Data;
using maketa_models.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace maketa_dataaccess.Repositories
{
    public class AuthRepository : IDisposable
    {
        public AuthRepository()
        {
            _ctx = new ApplicationDbContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_ctx));
        }

        public async Task<IdentityResult> RegisterUserAsync(UserModel userModel)
        {
            IdentityUser user = new IdentityUser {
                UserName = userModel.UserName
            };
            var result = await _userManager.CreateAsync(user, userModel.Password);
            return result;
        }

        public async Task<IdentityUser> FindUserAsync(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);
            return user;
        }

        public void Dispose()
        {
            _userManager.Dispose();
            _ctx.Dispose();
        }

        private ApplicationDbContext        _ctx;
        private UserManager<IdentityUser>   _userManager;
    }
}