﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using maketa_models.Models;
using Newtonsoft.Json.Linq;

namespace maketa_dataaccess.Repositories
{

    public class GranoRepository : IDisposable
    {
        public GranoRepository(string baseUri)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(baseUri);
        }

        public bool Authorize(string userName, string password)
        {
            // see: http://www.codeproject.com/Articles/823263/ASP-NET-Identity-Introduction-to-Working-with-Iden#Getting-Started---Create-a-New-ASP-NET-Web-Api-Project

            var pairs = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", userName),
                new KeyValuePair<string, string>("password", password)
            };
            String result = String.Empty;
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = _client.BaseAddress;
                    var response = client.PostAsync("/token", new FormUrlEncodedContent(pairs)).Result;
                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                        return false;

                    _lastResult = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    response.EnsureSuccessStatusCode();
                    _token = _lastResult["access_token"].ToString();
                }
            } catch (Exception e) {
                Trace.TraceError(@"error in Authorize: 
    Exception: {0}
    result: {1}", e, result);
                return false;
            }
            return true;
        }

        public void Clear()
        {
            _token      = null;
            _lastResult = null;
        }

        public bool Register(string username, string password, string confirmPassword)
        {
            var registerModel = new {
                UserName = username,
                Password = password,
                ConfirmPassword = password
            };
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = _client.BaseAddress;
                    var response = client.PostAsJsonAsync("/api/Account/Register", registerModel).Result;
                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                        return false;
                    _lastResult = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    response.EnsureSuccessStatusCode();
                }
            } catch (Exception e) {
                Trace.TraceError(@"error in Register: 
    Exception: {0}", e);
                return false;
            }
            return true;
        }

        public string Get(string id)
        {
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
            var response    = _client.GetAsync($"/api/grano/{id}").Result;
            
            var content     = response.Content.ReadAsStringAsync().Result;
            return content;
        }

        public string Save(Grano grano)
        {
            var jgrano      = JObject.FromObject(grano);
            var response    = _client.PostAsync($"/api/grano", new StringContent(jgrano.ToString(), Encoding.UTF8, "application/json")).Result;
            
            var content     = response.Content.ReadAsStringAsync().Result;
            return content;
        }

        public void Dispose()
        {
            if (_client != null)
                _client.Dispose();
        }

        public String FailReason {
            get
            {
                if (_lastResult["message"] != null) {
                    return _lastResult.ToString();
                } else if (_lastResult["error"] != null) {
                    return String.Format("{0}: {1}", _lastResult["error"], _lastResult["error_description"]);
                }
                return String.Empty;
            }
        }

        public bool IsAuthenticated { get { return !String.IsNullOrWhiteSpace(_token); } }

        readonly    HttpClient  _client;
                    string      _token;
                    JObject     _lastResult;
    }
}