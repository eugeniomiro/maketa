﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace maketa_dataaccess.Data
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext() 
            : base("ApplicationDb")
        {
        }
    }
}