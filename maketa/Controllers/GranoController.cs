﻿using System.Web.Http;
using maketa_models.Models;

namespace maketa.Controllers
{
    [Authorize]
    public class GranoController : ApiController
    {
        public IHttpActionResult Get(string id)
        {
            return Ok(new { saludo = $"Hola {id}" });
        }

        public IHttpActionResult Post(Grano grano)
        {
            return Ok(new { creado = $"Listo {grano.Name}" });
        }
    }
}
