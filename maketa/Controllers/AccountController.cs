﻿using System.Threading.Tasks;
using System.Web.Http;
using maketa_dataaccess.Repositories;
using maketa_models.Models;
using Microsoft.AspNet.Identity;

namespace maketa.Controllers
{
    public class AccountController : ApiController
    {
        public AccountController()
        {
            _repo = new AuthRepository();
        }

        public async Task<IHttpActionResult> Register(UserModel userModel)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            IdentityResult result = await _repo.RegisterUserAsync(userModel);
            IHttpActionResult errorResult = GetErrorResult(result);
            if (errorResult != null) {
                return errorResult;
            }
            return Ok();
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null) {
                return InternalServerError();
            }
            if (!result.Succeeded) {
                if (result.Errors != null) {
                    foreach (var error in result.Errors) {
                        ModelState.AddModelError("", error);
                    }
                }
                if (ModelState.IsValid) {
                    return BadRequest();
                }
                return BadRequest(ModelState);
            }
            return null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                _repo.Dispose();
            }

            base.Dispose(disposing);
        }

        private AuthRepository _repo;
    }
}
