﻿using System.Security.Claims;
using System.Threading.Tasks;
using maketa_dataaccess.Repositories;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;

namespace maketa
{
    internal class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
#pragma warning disable CS1998
// remove this warning disable clause and its counterpart when await code is added to ValidateClientAuthentication override!!

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

#pragma warning restore CS1998

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            using (var repo = new AuthRepository()) {
                IdentityUser user = await repo.FindUserAsync(context.UserName, context.Password);
                if (user == null) {
                    context.SetError("authentication failed", "The user name or password is incorrect");
                    return;
                }
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("sub", context.UserName));
            identity.AddClaim(new Claim("role", "user"));

            context.Validated(identity);
        }
    }
}