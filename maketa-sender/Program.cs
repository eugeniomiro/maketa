﻿using System;
using System.Configuration;
using maketa_dataaccess.Repositories;
using maketa_models.Models;
using static System.Console;

namespace maketa_sender
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0) return;

            var id              = args[0];
            using (var granoRepository = new GranoRepository(ConfigurationManager.AppSettings["baseAddress"] ?? "http://localhost:59547")) {

                if (!RegisterLogon(granoRepository)) return;

                WriteLine(granoRepository.Get(id));

                var grano           = new Grano { Name = "Soja" };
                WriteLine(granoRepository.Save(grano));
            }
        }

        private static bool RegisterLogon(GranoRepository repository)
        {
            var success = false;
            while (!success) {
                string input;
                do {
                    Write("logon or register [L/R] (ENTER abandon): ");
                    input = ReadLine();
                    if (input.Length == 0) {
                        return false;
                    }
                    switch (input.ToLower()) {
                        case "l":
                        case LogonCommand:
                            input = LogonCommand;
                            break;
                        case "r":
                        case RegisterCommand:
                            input = RegisterCommand;
                            break;
                        default:
                            break;
                    }
                } while (input != LogonCommand && input != RegisterCommand);

                Write("username (ENTER abandon): ");
                var username = ReadLine();
                if (username.Length == 0)
                    return success;

                Write("password: ");
                var password = ReadPassword();

                if (input == RegisterCommand) {
                    Write("confirm password: ");
                    var confirmPassword = ReadPassword();
                    success = repository.Register(username, password, confirmPassword);
                    if (!success) {
                        WriteLine("registration failed!: {0}", repository.FailReason);
                    }
                }
                success = repository.Authorize(username, password);
                if (!success) {
                    WriteLine("logon failed!!: {0}", repository.FailReason);
                }
            }
            return success;
        }

        // see: http://stackoverflow.com/a/3404522/41236
        private static string ReadPassword()
        {
            string pass = "";
            ConsoleKeyInfo key;

            do {
                key = ReadKey(true);

                switch (key.Key) {
                    case ConsoleKey.Backspace:
                        if (pass.Length > 0) {
                            pass = pass.Substring(0, pass.Length - 1);
                            Write("\b \b");
                        }
                        break;
                    case ConsoleKey.Enter:
                        WriteLine();
                        break;
                    default:
                        pass += key.KeyChar;
                        Write("*");
                        break;
                }
            }
            // Stops Receving Keys Once Enter is Pressed
            while (key.Key != ConsoleKey.Enter);
            return pass;
        }

        const string RegisterCommand    = "register";
        const string LogonCommand       = "logon";
    }
}
