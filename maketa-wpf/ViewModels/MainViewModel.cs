﻿using System;
using System.ComponentModel;
using System.Configuration;
using maketa_dataaccess.Repositories;

namespace maketa_wpf.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            _repository = new GranoRepository(ConfigurationManager.AppSettings["baseAddress"] ?? "http://localhost:59547");
            LogOnOff    = new RelayCommand(ExecuteLogOnOff);
        }

        public  RelayCommand    LogOnOff        { get; private set; }
        public  bool            IsConnected     
        {
            get { return !_repository.IsAuthenticated; }
        }
        public  string          UserName        { get; set; }
        public  string          Password        { get; set; }
        public  string          LogonButtonText { get { return _repository.IsAuthenticated ? "Log off" : "Log on"; } }

        public  event PropertyChangedEventHandler PropertyChanged;

        void ExecuteLogOnOff(object obj)
        {
            Boolean succeeded;
            if (!_repository.IsAuthenticated) {
                succeeded = _repository.Authorize(UserName, Password);
            } else {
                _repository.Clear();
                succeeded = true;
                UserName = String.Empty;
                Password = String.Empty;
                if (PropertyChanged != null) {
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs("UserName"));
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Password"));
                }
            }
            if (succeeded && PropertyChanged != null) {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsConnected"));
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs("LogonButtonText"));
            }
        }

        GranoRepository _repository;
    }
}
