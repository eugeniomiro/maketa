﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace maketa_wpf.ViewModels
{
    public static class PasswordAssistant
    {
        // Using a DependencyProperty as the backing store for BindPassword.  This enables animation, styling, binding, etc...
        public static void SetBindPassword(DependencyObject dp, bool value)
        {
            dp.SetValue(BindPassword, value);
        }

        public static bool GetBindPassword(DependencyObject dp)
        {
            return (bool) dp.GetValue(BindPassword);
        }

        public static readonly DependencyProperty BindPassword = DependencyProperty.RegisterAttached("BindPassword", 
                                                                                                      typeof(bool), 
                                                                                                      typeof(PasswordAssistant), 
                                                                                                      new PropertyMetadata(false, OnBindPasswordChanged));

        // Using a DependencyProperty as the backing store for BoundPassword.  This enables animation, styling, binding, etc...
        public static String GetBoundPassword(DependencyObject obj)
        {
            return (String) obj.GetValue(BoundPassword);
        }

        public static void SetBoundPassword(DependencyObject obj, String value)
        {
            obj.SetValue(BoundPassword, value);
        }

        public static readonly DependencyProperty BoundPassword = DependencyProperty.RegisterAttached("BoundPassword", 
                                                                                                      typeof(String), 
                                                                                                      typeof(PasswordAssistant), 
                                                                                                      new PropertyMetadata(String.Empty, BoundPasswordChanged));
        public static bool GetUpdatingPassword(DependencyObject obj)
        {
            return (bool) obj.GetValue(UpdatingPassword);
        }

        public static void SetUpdatingPassword(DependencyObject obj, bool value)
        {
            obj.SetValue(UpdatingPassword, value);
        }

        // Using a DependencyProperty as the backing store for UpdatingPassword.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UpdatingPassword = DependencyProperty.RegisterAttached("UpdatingPassword", 
                                                                                                         typeof(bool), 
                                                                                                         typeof(PasswordAssistant), 
                                                                                                         new PropertyMetadata(false));

        static void BoundPasswordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PasswordBox box = d as PasswordBox;

            // only handle this event when the property is attached to a PasswordBox
            // and when the BindPassword attached property has been set to true
            if (d == null || !GetBindPassword(d)) {
                return;
            }

            // avoid recursive updating by ignoring the box's changed event
            box.PasswordChanged -= HandlePasswordChanged;

            string newPassword = (string)e.NewValue;

            if (!GetUpdatingPassword(box)) {
                box.Password = newPassword;
            }

            box.PasswordChanged += HandlePasswordChanged;
        }

        static void OnBindPasswordChanged(DependencyObject dp, DependencyPropertyChangedEventArgs e)
        {
            // when the BindPassword attached property is set on a PasswordBox,
            // start listening to its PasswordChanged event

            PasswordBox box = dp as PasswordBox;

            if (box == null) {
                return;
            }

            bool wasBound = (bool)(e.OldValue);
            bool needToBind = (bool)(e.NewValue);

            if (wasBound) {
                box.PasswordChanged -= HandlePasswordChanged;
            }

            if (needToBind) {
                box.PasswordChanged += HandlePasswordChanged;
            }
        }

        static void HandlePasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox box = sender as PasswordBox;

            // set a flag to indicate that we're updating the password
            SetUpdatingPassword(box, true);
            // push the new password into the BoundPassword property
            SetBoundPassword(box, box.Password);
            SetUpdatingPassword(box, false);
        }
    }
}
