﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace maketa_wpf.ViewModels
{
    // see: https://msdn.microsoft.com/en-us/magazine/dd419663.aspx
    public class RelayCommand : ICommand
    {
        public RelayCommand(Action<object> execute) : this(execute, null) { }
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");
            _execute    = execute;
            _canExecute = canExecute;
        }

        [DebuggerStepThrough]
        public bool CanExecute(object parameter)    =>  _canExecute == null ? true : _canExecute(parameter);
        public void Execute(object parameter)       => _execute(parameter);

        public event EventHandler CanExecuteChanged
        {
            add     { CommandManager.RequerySuggested += value; }
            remove  { CommandManager.RequerySuggested -= value; }
        }

        readonly    Action<object>      _execute;
        readonly    Predicate<object>   _canExecute;
    }
}