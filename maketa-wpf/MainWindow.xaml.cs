﻿using System;
using System.Configuration;
using System.Windows;
using maketa_dataaccess.Repositories;
using maketa_models.Models;

namespace maketa_wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable
    {
        public MainWindow()
        {
            InitializeComponent();

            var grano   = new Grano { Name = "Soja" };

            LeftCell.Text   = _granoRepository.Get("Agustín");
            RightCell.Text  = _granoRepository.Save(grano);
            Status.Content  = _granoRepository.IsAuthenticated ? "Authenticated" : "Non authenticated";
        }

        public void Dispose()
        {
            _granoRepository.Dispose();
        }

        GranoRepository _granoRepository = new GranoRepository(ConfigurationManager.AppSettings["baseAddress"] ?? "http://localhost:59547");
    }
}
